var { defineSupportCode } = require('cucumber');
var { expect } = require('chai');

defineSupportCode(({ Given, When, Then }) => {
    Given('I go to losestudiantes home screen', () => {
        browser.url('/');
        if (browser.isVisible('button=Cerrar')) {
            browser.click('button=Cerrar');
        }
    });

    When('I open the login screen', () => {
        browser.pause(1000);
        browser.waitForVisible('button=Ingresar', 5000);
        browser.waitForEnabled("button=Ingresar", 5000);
        browser.click('button=Ingresar');
    });

    When('I fill a wrong email and password', () => {
        var cajaLogIn = browser.element('.cajaLogIn');

        var mailInput = cajaLogIn.element('input[name="correo"]');
        mailInput.click();
        mailInput.keys('wrongemail@example.com');

        var passwordInput = cajaLogIn.element('input[name="password"]');
        passwordInput.click();
        passwordInput.keys('123467891')
    });

    When('I try to login', () => {
        var cajaLogIn = browser.element('.cajaLogIn');
        cajaLogIn.element('button=Ingresar').click()
    });
    When('I try to sign-up', () => {
        var cajaSignUp = browser.element('.cajaSignUp');
        var buttonAceptar = cajaSignUp.element('.logInButton');
        buttonAceptar.click();
    });

    Then('I expect to not be able to login', () => {
        browser.waitForVisible('.aviso.alert.alert-danger', 5000);
    });

    When(/^I fill with (.*) and (.*)$/, (email, password) => {
        var cajaLogIn = browser.element('.cajaLogIn');

        browser.pause(500);
        var mailInput = cajaLogIn.element('input[name="correo"]');
        mailInput.click();
        mailInput.keys(email);

        browser.pause(500);
        var passwordInput = cajaLogIn.element('input[name="password"]');
        passwordInput.click();
        passwordInput.keys(password)
    });

    When('I expect to see {string}', error => {
        browser.waitForVisible('.aviso.alert.alert-danger', 40000);
        var alertText = browser.element('.aviso.alert.alert-danger').getText();
        expect(alertText).to.include(error);
    });

    When('I fill with used email', () => {
        browser.waitForVisible('input[name="nombre"]', 20000);
        var cajaSignUp = browser.element('.cajaSignUp');

        var nombreInput = cajaSignUp.element('input[name="nombre"]');
        nombreInput.click();
        nombreInput.keys('Servio');

        var apellidoInput = cajaSignUp.element('input[name="apellido"]');
        apellidoInput.click();
        apellidoInput.keys('Pantoja');

        browser.pause(500);
        var emailInput = cajaSignUp.element('input[name="correo"]');
        emailInput.click();
        emailInput.keys('shd_cristo@hotmail.com');

        browser.pause(500);
        var selectUniversidad = cajaSignUp.element('select[name="idUniversidad"]');
        selectUniversidad.selectByVisibleText("Universidad de los Andes");

        browser.pause(500);
        var selectPrograma = cajaSignUp.element('select[name="idPrograma"]');
        selectPrograma.selectByVisibleText("Administración");

        browser.pause(500);
        var passwordInput = cajaSignUp.element('input[name="password"]');
        passwordInput.click();
        passwordInput.keys('ServioPantoja');

        browser.pause(500);
        var checkAceptar = cajaSignUp.element('input[name="acepta"]');
        checkAceptar.click();
    });

    When('I fill valid user', () => {
        browser.waitForVisible('input[name="nombre"]', 20000);
        var cajaSignUp = browser.element('.cajaSignUp');

        var nombreInput = cajaSignUp.element('input[name="nombre"]');
        nombreInput.click();
        nombreInput.keys('Servio');

        var apellidoInput = cajaSignUp.element('input[name="apellido"]');
        apellidoInput.click();
        apellidoInput.keys('Pantoja');

        browser.pause(500);
        var emailInput = cajaSignUp.element('input[name="correo"]');
        emailInput.click();
        emailInput.keys(new Date().getTime() + '@hotmail.com');

        browser.pause(500);
        var selectUniversidad = cajaSignUp.element('select[name="idUniversidad"]');
        selectUniversidad.selectByVisibleText("Universidad de los Andes");

        browser.pause(500);
        var selectPrograma = cajaSignUp.element('select[name="idPrograma"]');
        selectPrograma.selectByVisibleText("Administración");

        browser.pause(500);
        var passwordInput = cajaSignUp.element('input[name="password"]');
        passwordInput.click();
        passwordInput.keys('ServioPantoja');

        browser.pause(500);
        var checkAceptar = cajaSignUp.element('input[name="acepta"]');
        checkAceptar.click();
    });

    Then('I would be able to see account menu', () => {
        browser.waitForVisible('#cuenta', 5000);
        isExisting = browser.isExisting('#cuenta');
        expect(isExisting).to.true;
    });

    Then('I expect error activating account', () => {
        browser.waitForVisible('.sweet-alert', 20000);
        var alertText = browser.element('.sweet-alert h2').getText();
        expect(alertText).to.include('Ocurrió un error activando tu cuenta');

    });
    Then('I expect register success message', () => {
        browser.waitForVisible('.sweet-alert', 20000);
        var alertText = browser.element('.sweet-alert h2').getText();
        expect(alertText).to.include('Registro exitoso!');

    });

});