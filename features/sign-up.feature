Feature: Sign up into losestudiantes
    As an user i want to be registered into los estudiantes

    Scenario: Register error, email is in use
        Given I go to losestudiantes home screen
        When I open the login screen
        And I fill with used email
        And I try to sign-up
        Then I expect error activating account

    Scenario: Register success
        Given I go to losestudiantes home screen
        When I open the login screen
        And I fill valid user
#And I try to sign-up
#Then I expect register success message